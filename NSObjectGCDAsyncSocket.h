//
//  NSObjectGCDAsyncSocket.h
//  attendance
//
//  Created by dgc on 15/7/7.
//  Copyright (c) 2015年 dgc. All rights reserved.
/**
 *  osChina数据拉取测试
 *
 *  @param NSUInteger
 *  @param GCDSocketType 
 *
 *  @return
 */
//GCDAsyncSocketDelegate执行代理对象

#import <Foundation/Foundation.h>
#import "Coc﻿oaAsy﻿ncSocket.h"
#import "Public.h"
#import <UIKit/UIKit.h>
/**
 *  socket适用范围
 */
typedef NS_ENUM(NSUInteger, GCDSocketType)
{
    /**
     *  默认
     */
    GCDSocketTypeDefault,
    /**
     *  聊天
     */
    GCDSocketTypeChat,
};

typedef void(^DidReadData)(NSDictionary* didReadData);
/**
 *  GCDAsyncSocketDelegate执行代理对象
 */
@interface NSObjectGCDAsyncSocket : NSObject<GCDAsyncSocketDelegate>
{
    /**
     *  socket
     */
 @public  GCDAsyncSocket* socket;

}
/**
 *  接收到数据的处理
 */
@property(nonatomic,copy)DidReadData didReadData;
/**
 *  发送的数据  如果添加新键值则需要先开辟内存
 */
@property(nonatomic,retain)NSMutableDictionary* writeData;
/**
 *  port
 */
@property(nonatomic,assign)uint16_t GCDASPort;
/**
 *  host
 */
@property(nonatomic,retain)NSString* GCDASHost;
/**
 *  socket适用范围
 */
@property(nonatomic,assign)GCDSocketType GCDSocketType0;
/**
 *  心跳包
 */
@property(nonatomic,retain)NSTimer* GCDTimer;
/**
 *  发送链接请求
 */
-(void)startConnect;
/**
 *  单例
 */
+(NSObjectGCDAsyncSocket*)defaultSocket;

@end
