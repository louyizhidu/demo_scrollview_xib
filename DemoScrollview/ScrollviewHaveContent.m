//
//  ScrollviewHaveContent.m
//  DemoRongCloud
//
//  Created by yuanweitiansheng on 16/3/16.
//  Copyright © 2016年 louyizhidu. All rights reserved.
//

#import "ScrollviewHaveContent.h"
@interface ScrollviewHaveContent ()
@end

@implementation ScrollviewHaveContent

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(nonnull instancetype)initWithNibName:(nullable NSString *)nibNameOrNil
                                bundle:(nullable NSBundle *)nibBundleOrNil
{
    self=[super initWithNibName:nibNameOrNil
                         bundle:nibBundleOrNil];
    return [[NSBundle mainBundle] loadNibNamed:@"ScrollviewHaveContent"
                                         owner:self
                                       options:nil][0];
}
@end
