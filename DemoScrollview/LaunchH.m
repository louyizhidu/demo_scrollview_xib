//
//  LaunchH.m
//  DemoRongCloud
//
//  Created by louyizhidu on 17/4/20.
//  Copyright © 2017年 louyizhidu. All rights reserved.
//

#import "LaunchH.h"

@interface LaunchH ()

@end

@implementation LaunchH

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self=[super initWithNibName:nibNameOrNil
                         bundle:nibBundleOrNil];
    if (self)
    {
        /**
         *  如果不想上下滚动需要(在xib中关闭adjsut scrolview insets/self.automaticallyAdjustsScrollViewInsets=NO;)
         *  first的Y轴对其就决定了水平滚动,追加view是对齐补充
         */
        return [[[NSBundle mainBundle] loadNibNamed:@"LaunchH"
                                             owner:self
                                           options:nil] firstObject];
    }
    return self;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
