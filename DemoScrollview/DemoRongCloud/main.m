//
//  main.m
//  DemoRongCloud
//
//  Created by louyizhidu on 16/3/2.
//  Copyright © 2016年 louyizhidu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
