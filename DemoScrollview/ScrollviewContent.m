//
//  ScrollviewContent.m
//  DemoRongCloud
//
//  Created by yuanweitiansheng on 16/3/15.
//  Copyright © 2016年 louyizhidu. All rights reserved.
//

#import "ScrollviewContent.h"

@implementation ScrollviewContent
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if (self)
    {
        UIView* base=[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                                   owner:self
                                                 options:nil] firstObject];
        base.frame=self.bounds;
        [self addSubview:base];
    }
    return self;
}
@end
